<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/**
 * Redirect from '/' to '/app/'.
 * The trailing slash is important because fly.io otherwise messes up the
 * redirect (and redirects to port 8080 for some reason).
 */
Route::get('/', function() {
    return redirect()->to('/api');
});
