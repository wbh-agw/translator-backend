<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/', function() {
    return response()->json([
        'env' => config('app.env'),
        'version' => config('app.version'),
        'url' => app('url')->to(''),
    ]);
});

Route::post('/translate', function (Request $req) {
    // Define validation rules
    $rules = [
        'text' => 'required',
    ];

    // Validate data
    $validator = Validator::make($req->all(), $rules);
    if ($validator->fails()) {
        return Response::json($validator->errors(), 400);
    }

    // Translate text
    $text = $req->json()->get('text');
    $translation = strtoupper($text);

    // Return result
    return response()->json([
        'text'          => $text,
        'translation'   => $translation,
    ]);
});

Route::post('/download', function (Request $req) {
    // Define validation rules
    $rules = [
        'text' => 'required',
        'translation' => 'required',
    ];

    // Validate data
    $validator = Validator::make($req->all(), $rules);
    if ($validator->fails()) {
        return Response::json($validator->errors(), 400);
    }

    // Get data
    $text = $req->json()->get('text');
    $translation = $req->json()->get('translation');

    // Build text
    $txt = "Original Text:\n" . $text . "\n\nTranslation:\n" . $translation;

    // Return file download
    return response()->streamDownload(function () use ($txt) {
        echo $txt;

    }, 'translation.txt');
});
