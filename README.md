# Translator Backend

# Development
## Prerequisites
-   Docker (WSL2 works too)
-   Composer (https://getcomposer.org)

## Getting started
-   Clone this repository and `cd` into it
-   `composer install`
-   `sail up` or `vendor/bin/sail up` (if you don't have the console alias)
-   Test the API by requesting `GET http://localhost/api`. This will give
    you information about the API (version, ...).

## Useful commands
```bash
# Start backend
sail up -d          # alternatively: vendor/bin/sail up -d

# Stop backend
sail down           # alternatively: vendor/bin/sail down

# Initial deploy
fly launch

# Subsequent deployments
fly deploy

```

## Environment
When the application is started locally, the `.env` file is loaded which
contains the following environment variables:
-   `APP_ENV`: this sets the environment of the application.

    In local development the `APP_ENV` variable is filled with the value `local`
    (which is provided in `.env`).

    In the production environment (fly.io) `APP_ENV` is set through the fly.io
    environment (which is defined in `fly.toml`) with the value `production`.

    The `APP_ENV` variable is only used to set up CORS correctly depending
    on the environment the application is executed in.

-   `APP_DEBUG`: this controls the debug logs of the application.

    In local development the `APP_DEBUG` variable has the value `true`
    which is loaded from `.env`.

    In the production environment (fly.io) `APP_DEBUG` is not set and therefore
    it defaults to false (see `config/app.php`).
